package ru.ekfedorov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.dto.SessionDTO;
import ru.ekfedorov.tm.dto.TaskDTO;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.NullSessionException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @NotNull
    @WebMethod
    TaskDTO addTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void bindTaskByProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String taskId
    ) throws AccessDeniedException, NullSessionException;

    void changeTaskStatusById(
            @NotNull SessionDTO session,
            @NotNull String id,
            @NotNull Status status
    ) throws AccessDeniedException, NullSessionException;

    void changeTaskStatusByIndex(
            @NotNull SessionDTO session,
            @NotNull Integer index,
            @NotNull Status status
    ) throws AccessDeniedException, NullSessionException;

    void changeTaskStatusByName(
            @NotNull SessionDTO session,
            @NotNull String name,
            @NotNull Status status
    ) throws AccessDeniedException, NullSessionException;

    @NotNull
    @WebMethod
    List<TaskDTO> findAllByProjectId(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId
    ) throws AccessDeniedException, NullSessionException;

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws AccessDeniedException, NullSessionException ;

    @WebMethod
    void clearBySessionTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws AccessDeniedException, NullSessionException;

    @NotNull
    @WebMethod
    List<TaskDTO> findTaskAllWithComparator(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "sort", partName = "sort") @NotNull String sort
    ) throws AccessDeniedException, NullSessionException;

    @Nullable
    @WebMethod
    TaskDTO findTaskOneById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws AccessDeniedException, NullSessionException;

    @Nullable
    @WebMethod
    TaskDTO findTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    ) throws AccessDeniedException, NullSessionException;

    @Nullable
    @WebMethod
    TaskDTO findTaskOneByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    ) throws AccessDeniedException, NullSessionException;

    void finishTaskById(
            @NotNull SessionDTO session,
            @NotNull String id
    ) throws AccessDeniedException, NullSessionException;

    void finishTaskByIndex(
            @NotNull SessionDTO session,
            @NotNull Integer index
    ) throws AccessDeniedException, NullSessionException;

    void finishTaskByName(
            @NotNull SessionDTO session,
            @NotNull String name
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void removeTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "task", partName = "task") @NotNull TaskDTO task
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void removeTaskOneById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void removeTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void removeTaskOneByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    ) throws AccessDeniedException, NullSessionException;

    void startTaskById(
            @NotNull SessionDTO session,
            @NotNull String id
    ) throws AccessDeniedException, NullSessionException;

    void startTaskByIndex(
            @NotNull SessionDTO session,
            @NotNull Integer index
    ) throws AccessDeniedException, NullSessionException;

    void startTaskByName(
            @NotNull SessionDTO session,
            @NotNull String name
    ) throws AccessDeniedException, NullSessionException;

    @WebMethod
    void unbindTaskFromProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String taskId
    ) throws AccessDeniedException, NullSessionException;

    void updateTaskById(
            @NotNull SessionDTO session,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    ) throws AccessDeniedException, NullSessionException;

    void updateTaskByIndex(
            @NotNull SessionDTO session,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description
    ) throws AccessDeniedException, NullSessionException;

}
